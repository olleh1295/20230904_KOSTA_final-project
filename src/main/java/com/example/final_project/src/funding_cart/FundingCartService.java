package com.example.final_project.src.funding_cart;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.jwt.JwtUtil;
import com.example.final_project.src.funding.entity.Funding;
import com.example.final_project.src.funding.repository.FundingRepository;
import com.example.final_project.src.funding_cart.entity.FundingCart;
import com.example.final_project.src.funding_cart.entity.FundingCartItem;
import com.example.final_project.src.funding_cart.repository.FundingCartItemRepository;
import com.example.final_project.src.funding_cart.repository.FundingCartRepository;
import com.example.final_project.src.user.UserRepository;
import com.example.final_project.src.user.entity.User;
import com.example.final_project.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Optional;

import static com.example.final_project.common.response.BaseResponseStatus.*;

@Transactional
@RequiredArgsConstructor
@Service
@Slf4j
public class FundingCartService {
    @Autowired
    private FundingCartRepository fundingCartRepository;
    @Autowired
    private FundingCartItemRepository fundingCartItemRepository;
    @Autowired
    private FundingRepository fundingRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtil jwtUtil;

    @Transactional
    public String addToCart(HttpServletRequest request, Long fundingId) {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태 예외 처리
        else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        // 유저네임을 이용하여 유저 객체 찾기
        String username = jwtUtil.extractUsername(jwtToken);

        // userId를 이용하여 User 찾기
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // fundingId를 이용하여 Funding 게시글 찾기
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        // User 객체를 이용하여 유저의 장바구니 객체 있는지 확인
        Optional<FundingCart> checkfundingCart = fundingCartRepository.findFundingCartByUser(user);
        FundingCart fundingCart = null;

        // 유저의 장바구니가 없다면 생성하기
        if(!checkfundingCart.isPresent()){
            // user의 장바구니 객체 생성 및 DB 저장
            fundingCart = new FundingCart(user);
            fundingCartRepository.save(fundingCart);
            fundingCart = fundingCartRepository.findFundingCartByUser(user).get();
        }
        // 유저의 장바구니가 있다면 장바구니 가져오기
        else {
            fundingCart = checkfundingCart.get();
        }


        // fundingCart 객체를 이용하여 기존 장바구니 추가된 아이템인지 확인
        Optional<FundingCartItem> checkfundingItem = fundingCartItemRepository.findFundingCartItemByFundingAndFundingCart(funding, fundingCart);
        FundingCartItem fundingCartItem = null;

        // 이미 장바구니에 담겨진 아이템이면 객체 가져오기
        if(checkfundingItem.isPresent()) {
            fundingCartItem = checkfundingItem.get();
        }

        // 장바구니에 없는 아이템이라면, FundingCartItem 생성
        else {
            // fundingCartItem 생성
            fundingCartItem = new FundingCartItem(fundingCart, funding);
            // fundingCartItem  저장
            fundingCartItemRepository.save(fundingCartItem);
            // 생성한 객체 가져오기
            fundingCartItem = fundingCartItemRepository.findFundingCartItemByFundingAndFundingCart(funding, fundingCart).get();
        }
        // 아이템 수량 하나 추가
        fundingCartItem.addQuantity(1);

        return "장바구니 추가가 완료되었습니다.";
    }



    @Transactional(readOnly = true)
    public List<FundingCartItem> getCartItems(HttpServletRequest request) {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태 예외 처리
        else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        // 유저네임을 이용하여 유저 객체 찾기
        String username = jwtUtil.extractUsername(jwtToken);

        // userId를 이용하여 User 찾기
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // User 객체를 이용하여 유저의 장바구니 목록 가져오기, 장바구니가 비어있으면 장바구니 생성
        Optional<FundingCart> checkfundingCart = fundingCartRepository.findFundingCartByUser(user);

        if (checkfundingCart.isEmpty()){
            FundingCart fundingCart = new FundingCart(user);
            return fundingCart.getFundingItems();
        } else {
            FundingCart fundingCart = checkfundingCart.get();
            return fundingCart.getFundingItems();
        }
    }


}
