package com.example.final_project.src.funding_cart;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.src.funding.FundingService;
import com.example.final_project.src.funding.entity.*;
import com.example.final_project.src.funding.model.*;
import com.example.final_project.src.funding_cart.entity.FundingCart;
import com.example.final_project.src.funding_cart.entity.FundingCartItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.final_project.common.response.BaseResponseStatus.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class FundingCartController {
    @Autowired
    private FundingCartService fundingCartService;


    /**
     * 로그인한 유저의 장바구니 목록 조회 API
     * [GET] /fundingcart/all
     * @param request HttpServletRequest
     * @return BaseResponse<List<FundingCartItem>>
     */
    @ResponseBody
    @GetMapping("/fundingcart/all")
    public BaseResponse<List<FundingCartItem>> getAllItemsByUser(HttpServletRequest request) {
        return new BaseResponse<>(fundingCartService.getCartItems(request));
    }



    /**
     * 로그인한 유저의 장바구니에 아이템 추가 API
     * [Get] /fundingcart/add
     * @param request HttpServletRequest
     * @param funding_id 펀딩 고유 ID
     * @return BaseResponse<List<FundingCartItem>>
     */
    @ResponseBody
    @GetMapping("/fundingcart/add")
    public BaseResponse<String> addItemByUser(HttpServletRequest request, @RequestParam(name = "funding") Long funding_id) {
        return new BaseResponse<>(fundingCartService.addToCart(request, funding_id));
    }




}