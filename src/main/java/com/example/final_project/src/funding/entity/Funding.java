package com.example.final_project.src.funding.entity;

import com.example.final_project.common.Constant;
import com.example.final_project.common.entity.BaseEntity;

import com.example.final_project.src.user.entity.User;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.sql.Clob;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(callSuper = false)
@Getter
@Entity // 필수, Class 를 Database Table화 해주는 것이다
@EntityScan
@Table(name = "Funding")// Table 이름을 명시해주지 않으면 class 이름을 Table 이름으로 대체한다.)
public class Funding extends BaseEntity {
    // FundingState를 사용하도록 정의
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false, length = 20)
    private Constant.FundingState state = Constant.FundingState.ACTIVE;

    @Id  // PK를 의미하는 어노테이션
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "num_sequence")
    @SequenceGenerator(name = "num_sequence", sequenceName = "NUM_SEQUENCE", allocationSize = 1)
    private Long id;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "subtitle", nullable = false, length = 50)
    private String subtitle;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id", nullable = false)
    private FundingCategory fundingCategory;

    @ManyToOne
    @JoinColumn(name = "writer_id", referencedColumnName = "id", nullable = false)
    private User writer;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "deadline", nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime deadline;

    @Column(name = "goal", nullable = false)
    private Long goal;

    @Column(name = "min_participants", nullable = false)
    private Long minParticipants;

    @Column(name = "max_participants", nullable = false)
    private Long maxParticipants;

    @Column(name = "amount", nullable = false)
    private Long amount;


    @Column(name = "like_count", nullable = false)
    private Long likeCount = 0L;

    @Column(name = "view_count", nullable = false)
    private Long viewCount = 0L;

    @Lob // CLOB 형식 컬럼
    @Column(name = "thumbnail", nullable = false)
    private String thumbnail;

    @Lob
    @Column(name = "content", nullable = false) // 본문 이미지와 본문 내용을 base64 인코딩 데이터 그대로 저장
    private String content;

    @Builder
    public Funding(String title, String subtitle, FundingCategory fundingCategory, User writer, String location, LocalDateTime deadline,
                   Long goal, Long minParticipants, Long maxParticipants, Long amount, Long likeCount, Long viewCount, String thumbnail, String content) {
        this.title = title;
        this.subtitle = subtitle;
        this.fundingCategory = fundingCategory;
        this.writer = writer;
        this.location = location;
        this.deadline = deadline;
        this.goal = goal;
        this.minParticipants = minParticipants;
        this.maxParticipants = maxParticipants;
        this.amount = amount;
        this.likeCount = 0L;   // 기본값 0으로 설정
        this.viewCount = 0L;   // 기본값 0으로 설정
        this.thumbnail = thumbnail;
        this.content = content;
    }


    public void updateThumnail(String thumbnail){ this.thumbnail = thumbnail;}

    public void updateContent(String content) { this.content = content;}

    public void updateCategory(FundingCategory fundingCategory){
        this.fundingCategory = fundingCategory;
    }

    public void updateWriter(User writer){
        this.writer = writer;
    }

    public void addViewCount(){this.viewCount += 1;}

    public void updateState(Constant.FundingState state) {this.state = state; }
}