package com.example.final_project.src.funding;

import com.example.final_project.common.Constant;
import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.jwt.JwtUtil;
import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.src.funding.entity.*;
import com.example.final_project.src.funding.model.CommentInsertReq;
import com.example.final_project.src.funding.model.FundingInsertReq;
import com.example.final_project.src.funding.model.FundingInsertRes;
import com.example.final_project.src.funding.model.PaymentInsertReq;
import com.example.final_project.src.funding.repository.*;
import com.example.final_project.src.post_like.PostLikeRepository;
import com.example.final_project.src.post_like.entity.PostLike;
import com.example.final_project.src.user.UserRepository;
import com.example.final_project.src.user.entity.User;
import com.example.final_project.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

import static com.example.final_project.common.Constant.FundingState.ACTIVE;
import static com.example.final_project.common.Constant.FundingState.IN_PROGRESS;
import static com.example.final_project.common.response.BaseResponseStatus.*;

import javax.servlet.http.HttpServletRequest;

// Service Create, Update, Delete 의 로직 처리
@Transactional
@RequiredArgsConstructor
@Service
@EnableScheduling
@Slf4j
public class FundingService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FundingRepository fundingRepository;

    @Autowired
    private FundingCategoryRepository fundingCategoryRepository;

    @Autowired
    private FundingCommentRepository fundingCommentRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private PostLikeRepository postLikeRepository;

    @Autowired
    private FundingParticipantsRepository fundingParticipantsRepository;

    @Autowired
    private FundingPaymentRepository fundingPaymentRepository;

    @Autowired
    private JwtUtil jwtUtil;


    @Transactional
    public FundingInsertRes createFunding(HttpServletRequest request, FundingInsertReq fundingInsertReq) throws SQLException {
        // funding 객체를 DB에 저장
        Funding saveFunding = fundingRepository.save(fundingInsertReq.toEntity());

        // 1-1. category Id를 변수에 저장
        Long categoryId = fundingInsertReq.getCategory_id();
        log.info("게시글 작성 서비스, categoryId= {}", categoryId);
        // 1-2. catefory Id를 이용하여 카테고리명 객체
        FundingCategory category = fundingCategoryRepository.findFundingCategoriesById(categoryId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING_CATEGORY));
        log.info("게시글 작성 서비스, category= {}", category);
        // 1-3. 조회한 카테고리 객체를 funding 객체에 저장
        saveFunding.updateCategory(category);

        // 2-1. username을 변수에 저장
        // 쿠키에서 jwtToken 값 가져오기.
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
            String username = jwtUtil.extractUsername(jwtToken);

            //String writer = fundingInsertReq.getWriter();
            // 2-2. username을 이용하여 카테고리명 객체
            User user = userRepository.findUserByUsername(username)
                    .orElseThrow(() -> new BaseException(NOT_FIND_USER));
            // 2-3. 조회한 유저 객체를 funding 객체에 저장
            saveFunding.updateWriter(user);
        } else {
            //throw new BaseException(NOT_FIND_LOGIN_SESSION);
            // 테스트 임시로 user 임시 저장
            String username = "hjlee";
            // 2-2. username을 이용하여 카테고리명 객체
            User user = userRepository.findUserByUsername(username)
                    .orElseThrow(() -> new BaseException(NOT_FIND_USER));
            // 2-3. 조회한 유저 객체를 funding 객체에 저장
            saveFunding.updateWriter(user);
        }

        // 게시글 생성 완료 응답 모델 반환
        return new FundingInsertRes(saveFunding);
    }


    @Transactional(readOnly = true)
    public List<Funding> getAllFundings() {
        List<Funding> fundings =  fundingRepository.findAllBy();

        // 게시글 리스트 반환
        return fundings;
    }

    @Transactional(readOnly = true)
    public List<Funding> getFundingsBypage(List<Funding> fundings, int pageNum, int itemsPerPage) {
        // 페이지 번호가 0부터 시작하므로, 사용자 입력값인 pageNum을 1 빼줍니다.
        int page = pageNum - 1;

        // 전체 게시물 수
        int totalItems = fundings.size();

        // 시작 인덱스 계산
        int startIndex = page * itemsPerPage;

        // 끝 인덱스 계산
        int endIndex = Math.min(startIndex + itemsPerPage, totalItems);

        // 페이지네이션을 적용하여 해당 범위의 게시물 추출
        List<Funding> paginatedFundings = fundings.subList(startIndex, endIndex);

        return paginatedFundings;
    }

    // 펀딩 리스트 중 좋아요 누른 게시글 표시 기능
    @Transactional(readOnly = true)
    public List<Boolean> getLikedFundingList(HttpServletRequest request, List<Funding> fundingList) {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태로 인해 좋아요 기능 처리 불가
        else {
            // 반환할 리스트 확인 List<Boolean> boolList
            List<Boolean> booleanList = new ArrayList<>();//fundingList 길이만큼 생성
            // 매개변수로 받은 전체 펀딩 게시글에서
            for (Funding funding : fundingList) {
                booleanList.add(false);
            }
            // 게시글 리스트 반환
            return booleanList;
        }

        // 유저네임을 이용하여 유저 객체 찾기
        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // 반환할 리스트 확인 List<Boolean> boolList
        List<Boolean> booleanList = new ArrayList<>();//fundingList 길이만큼 생성

        // 매개변수로 받은 전체 펀딩 게시글에서
        for (Funding funding : fundingList) {
            // 게시글 id
            Long funding_id = funding.getId();
            // 유저 id, 게시글 id에 해당하는 좋아요 누른 펀딩 게시글 확인
            Optional<PostLike> postLike = postLikeRepository.findPostLikesByUserAndTargetTypeAndAndTargetId(user, Constant.TargetType.FUNDING, funding_id);
            if(postLike.isPresent()){
                booleanList.add(true);
            }
            else {
                booleanList.add(false);
            }
        }
        // 게시글 리스트 반환
        return booleanList;
    }



    @Transactional(readOnly = true)
    public List<Funding> getFundingsByUser(Long userId) {

        // userId를 이용하여 user 객체 가져오기
        User user = userRepository.findUserById(userId)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // 해당 user 객체를 가진 펀딩 게시글 리스트 찾기
        List<Funding> fundings =  fundingRepository.findFundingsByWriter(user);

        // 게시글 리스트 반환
        return fundings;
    }

    @Transactional(readOnly = true)
    public List<Funding> getFundingsByCategory(Long categoryId) {

        // fundingId를 이용하여 funding 카테고리 객체 가져오기
        FundingCategory fundingCategory = fundingCategoryRepository.findFundingCategoriesById(categoryId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING_CATEGORY));

        // 해당 카테고리를 지닌 게시글 리스트 반환
        return fundingRepository.findFundingsByFundingCategory(fundingCategory);
    }



    @Transactional(readOnly = true)
    public Funding getFundingByFunding(Long fundingId) {
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        // View Count 1 증가
        funding.addViewCount();

        // 게시글 반환
        return funding;
    }

    @Transactional(readOnly = true)
    public List<Funding> getFundingsByKeyword(String keyword) {

        // 결과를 저장할 리스트 생성
        List<Funding> combinedFundings = new ArrayList<>();

        // 1. 제목에서 키워드와 부분 일치하는 게시글 목록 가져오기
        List<Funding> fundings1 = fundingRepository.findFundingsByTitleContaining(keyword);
        combinedFundings.addAll(fundings1);

        // 2. 부제목에서 키워드와 부분 일치하는 게시글 목록 가져오기
        List<Funding> fundings2 = fundingRepository.findFundingsBySubtitleContaining(keyword);
        combinedFundings.addAll(fundings2);

        // 3. 카테고리명에서 키워드와 부분 일치하는 카테고리 객체 목록 가져오기
        List<FundingCategory> categories = fundingCategoryRepository.findFundingCategoriesByCategoryNameContaining(keyword);

        // 키워드를 지닌 카테고리명 검색 결과가 empty가 아닐 경우,
        if (!categories.isEmpty()) {
            for (FundingCategory category : categories){
                // 해당 카테고리에 속한 게시글 목록 가져오기
                List<Funding> fundings3 = fundingRepository.findFundingsByFundingCategory(category);
                combinedFundings.addAll(fundings3);
            }
        }

        // 중복 제거
        Set<Funding> uniqueFundings = new HashSet<>(combinedFundings);
        combinedFundings.clear();
        combinedFundings.addAll(uniqueFundings);

        // 게시글 리스트 반환
        return combinedFundings;
    }


    @Transactional
    public String updatePostLike (HttpServletRequest request, Long fundingId) {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태 예외 처리
        else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // user와 funding 객체를 이용하여 PostLike 찾기
        Optional<PostLike> checkPostLike = postLikeRepository.findPostLikesByUserAndTargetTypeAndAndTargetId(user, Constant.TargetType.FUNDING, fundingId);

        // 이미 좋아요를 누른 경우, 좋아요 삭제
        if (checkPostLike.isPresent()) {
            postLikeRepository.delete(checkPostLike.get());
            return "좋아요 취소가 완료하였습니다.";
        }
        // 좋아요가 없는 경우, 좋아요 등록
        else {
            PostLike postLike = new PostLike(user, Constant.TargetType.FUNDING, fundingId);
            postLikeRepository.save(postLike);
            return "좋아요가 등록되었습니다.";
        }
    }

    @Transactional(readOnly = true)
    public Long countFundingView (Long fundingId) {
        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        return funding.getViewCount();
    }

    @Transactional(readOnly = true)
    public Long countFundingLike (Long fundingId) {
        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        return funding.getLikeCount();
    }

    @Transactional(readOnly = true)
    public Long countFundingComment (Long fundingId) {
        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        return fundingCommentRepository.countFundingCommentByFunding(funding);
    }


    @Transactional(readOnly = true)
    public List<Funding> SortedByKeyword (String status, Long geo_num) {
        if (status != null){
            Constant.FundingState fundingState = Constant.FundingState.valueOf(status.toUpperCase());
            return fundingRepository.findFundingsByState(fundingState);
        }

        else if (geo_num != null) {
            // 지역 넘버를 이용하여 대한민국 시 가져오기
            City city = cityRepository.findCitiesByParentId(geo_num)
                    .orElseThrow(() -> new BaseException(NOT_FIND_CITY));

            // 지역 네임을 지닌 펀딩 게시글 리스트 반환
            return fundingRepository.findFundingsByLocation(city.getName());
        }
        return null;
    }


    @Transactional(readOnly = true)
    public List<Funding> OrderByKeyword (List<Funding> fundingList, String condition) {
        if (condition == null || condition.equals("newest")) {
            // 최신순 정렬
            fundingList.sort(Comparator.comparing(Funding::getCreatedAt).reversed());
        } else if (condition.equals("deadline")) {
            // 마감임박순 정렬
            LocalDateTime currentTime = LocalDateTime.now();
            fundingList.sort(Comparator.comparing(funding -> {
                LocalDateTime deadline = funding.getDeadline();
                return (deadline != null && deadline.isAfter(currentTime)) ? deadline : currentTime;
            }));
        } else if (condition.equals("mostLike")) {
            // 좋아요순 정렬
            fundingList.sort(Comparator.comparing(Funding::getLikeCount).reversed());
        } else {
            throw new BaseException(NOT_FIND_FUNDING);
        }
        return fundingList;

    }


    @Transactional(readOnly = true)
    public List<Funding> OrderByState (List<Funding> fundingList, String state) {
        if (state == null || state.isEmpty()) {
            // 상태가 지정되지 않은 경우, 원래 리스트를 그대로 반환
            return fundingList;
        }

        else {
            // String state가 상수값으로 등록된 상태값인지 확인
            try {
                Constant.FundingState fundingState = Constant.FundingState.valueOf(state.toUpperCase());

                switch (fundingState) {
                    case ACTIVE, IN_PROGRESS, DELETE, COMPLETE, FAIL -> {
                        // 입력된 상태값에 따라 필터링
                        List<Funding> filteredList = new ArrayList<>();
                        for (Funding funding : fundingList) {
                            if (funding.getState().equals(fundingState)) {
                                // 매개변수 state와 funding state가 일치하면 저장
                                filteredList.add(funding);
                            }
                        }
                        return filteredList;
                    }
                    default -> {
                        throw new BaseException(INVALID_STATE);
                    }
                }
            } catch (IllegalArgumentException e) {
                throw new BaseException(INVALID_STATE);
            }
        }

    }

    @Transactional
    public int calculateAchievementRate (Long funding_id) {
        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(funding_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        // 펀딩 게시글의 목표 인원
        Long goal = funding.getGoal();
        // 현재 참여 인원
        int currentPeople = fundingParticipantsRepository.countFundingParticipantsByFunding(funding);

        // 달성률 퍼센티지 반환 (반올림)
        return Math.round((float) currentPeople / goal * 100);
    }


    @Transactional(readOnly = true)
    public List<FundingCategory> getCategoryAllList () {
        return fundingCategoryRepository.findAll();
    }


    @Transactional(readOnly = true)
    public FundingCategory getCategoryById (Long catogoryId) {

        return fundingCategoryRepository.findFundingCategoriesById(catogoryId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING_CATEGORY));
    }


    @Transactional(readOnly = true)
    public List<FundingComment> getCommentByUser (HttpServletRequest request) {
        // 쿠키에서 jwtToken 값 가져오기.
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        } else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        return fundingCommentRepository.findFundingCommentsByWriter(user);

    }



    @Transactional(readOnly = true)
    public List<FundingComment> getCommentByFunding (Long fundingId) {
        // fundingId를 이용하여 funding 객체 가져오기
        Funding funding = fundingRepository.findFundingById(fundingId)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        return fundingCommentRepository.findFundingCommentsByFunding(funding);

    }


    @Transactional
    public String insertCommentByUser (HttpServletRequest request, CommentInsertReq commentInsertReq) {
        // 쿠키에서 jwtToken 값 가져오기.
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        } else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        // jwt 토큰을 이용하여 username 추출
        String username = jwtUtil.extractUsername(jwtToken);
        // username을 이용하여 user 찾기
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // 펀딩 고유 id 추출
        Long funding_id = commentInsertReq.getFunding_id();
        // 펀딩 id를 이용하여 funding 객체 찾기
        Funding funding = fundingRepository.findFundingById(funding_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

        // 댓글 모델 entity화 (String content 저장)
        FundingComment fundingComment = commentInsertReq.toEntity();
        // 댓글 모델에 참조할 펀딩 객체 업데이트
        fundingComment.updateFunding(funding);
        // 댓글 모델에 참조할 유저 객체 업데이트
        fundingComment.updateWriter(user);

        // 댓글 모델 DB 저장 완료
        fundingCommentRepository.save(fundingComment);

        return "댓글 쓰기가 완료되었습니다.";

    }



    @Transactional(readOnly = true)
    public List<FundingParticipants> getParticipantsByLoginUserAndStateAndFunding (HttpServletRequest request, String state, Long funding_id) {
        // 쿠키에서 jwtToken 값 가져오기.
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        } else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        // jwt 토큰을 이용하여 username 추출
        String username = jwtUtil.extractUsername(jwtToken);
        // username을 이용하여 user 찾기
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));


        // 펀딩 참여자 상태 및 펀딩 게시글 id가 null이 아닐때, (user_id, funding_id, 참여자 state로 검색)
        if (state != null && funding_id != null) {
            // fundingId를 이용하여 funding 객체 가져오기
            Funding funding = fundingRepository.findFundingById(funding_id)
                    .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

            // String state가 상수값으로 등록된 상태값인지 확인
            try {
                Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());

                switch (fundingParticipantsState) {
                    case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                        return fundingParticipantsRepository.findFundingParticipantsByParticipantAndStateAndFunding(user, fundingParticipantsState, funding);
                    }
                    default -> {
                        throw new BaseException(INVALID_STATE);
                    }
                }
            } catch (IllegalArgumentException e) {
                throw new BaseException(INVALID_STATE);
            }
        } else {
            // 펀딩 참여자 상태 값이 null이 아닐때, (user_id, 참여자 state로 검색)
            if (state != null) {

                // String state가 상수값으로 등록된 상태값인지 확인
                try {
                    Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());

                    switch (fundingParticipantsState) {
                        case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                            return fundingParticipantsRepository.findFundingParticipantsByParticipantAndState(user, fundingParticipantsState);
                        }
                        default -> {
                            throw new BaseException(INVALID_STATE);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    throw new BaseException(INVALID_STATE);
                }
            }
            // 펀딩 id가 null이 아닐때, (user_id, funding id로 검색)
            else if (funding_id != null) {
                // fundingId를 이용하여 funding 객체 가져오기
                Funding funding = fundingRepository.findFundingById(funding_id)
                        .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

                // funding, user 객체를 활용하여 펀딩 참여자 리스트 가져오기
                Optional<FundingParticipants> optionalFundingParticipants = fundingParticipantsRepository.findFundingParticipantsByParticipantAndFunding(user, funding);

                if (optionalFundingParticipants.isPresent()) {
                    List<FundingParticipants> resultList = new ArrayList<>();
                    resultList.add(optionalFundingParticipants.get());
                    return resultList;
                } else {
                    // Optional에 값이 없는 경우 빈 리스트 반환
                    return Collections.emptyList();
                }

            } else {
                // user_id는 필수이므로, user_id만 넘어온 경우에는 userid로만 처리
                log.info("user= {}", user);
                return fundingParticipantsRepository.findFundingParticipantsByParticipant(user);
            }
        }
    }



    // user_id , state , funding_id 검색 옵션
    @Transactional(readOnly = true)
    public List<FundingParticipants> getParticipantsByUserAndStateAndFunding(Long user_id, String state, Long funding_id) {
        //1. user id 가 null 이 아닌 경우
        // 1-1. state가 null이고 funding_id가 null 인 경우
        // 1-2. state가 null이 아니고 funding_id가 null 인 경우
        // 1-1. state가 null이고 funding_id가 null이 아닌 경우

        //2. state 가 null 이 아닌 경우
        // 2-1. user_id null이고 funding_id가 null 인 경우
        // 2-2. user_id null이 아니고 funding_id가 null 인 경우
        // 2-1. user_id null이고 funding_id가 null이 아닌 경우

        //3. funding_id 가 null 이 아닌 경우
        // 2-1. user_id null이고 state null 인 경우
        // 2-2. user_id null이 아니고 state null 인 경우
        // 2-1. user_id null이고 state null이 아닌 경우


        //1. user id 가 null 이 아닌 경우
        if (user_id != null) {
            // userid를 이용하여 user 객체 찾기
            User user = userRepository.findUserById(user_id)
                    .orElseThrow(() -> new BaseException(NOT_FIND_USER));

            // user_id만 넘어온 경우에는 userid로만 처리
            if (state == null && funding_id == null) {
                return fundingParticipantsRepository.findFundingParticipantsByParticipant(user);
            }

            // user_id와 state를 이용하여 리스트 반환
            else if (state != null && funding_id == null) {
                try {
                    Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());

                    switch (fundingParticipantsState) {
                        case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                            return fundingParticipantsRepository.findFundingParticipantsByParticipantAndState(user, fundingParticipantsState);
                        }
                        default -> {
                            throw new BaseException(INVALID_STATE);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    throw new BaseException(INVALID_STATE);
                }
            }

            // user_id와 state, funding_id 를 이용하여 리스트 반환
            else {
                try {
                    Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());
                    // fundingId를 이용하여 funding 객체 가져오기
                    Funding funding = fundingRepository.findFundingById(funding_id)
                            .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

                    switch (fundingParticipantsState) {
                        case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                            return fundingParticipantsRepository.findFundingParticipantsByParticipantAndStateAndFunding(user, fundingParticipantsState, funding);
                        }
                        default -> {
                            throw new BaseException(INVALID_STATE);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    throw new BaseException(INVALID_STATE);
                }
            }
        }

        //2. state 가 null 이 아닌 경우
        if (state != null) {
            Constant.FundingParticipantsState fundingParticipantsState = null;
            try {
                switch (Constant.FundingParticipantsState.valueOf(state.toUpperCase())) {
                    case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                        fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());
                    }
                    default -> {
                        throw new BaseException(INVALID_STATE);
                    }
                }
            } catch (IllegalArgumentException e) {
                throw new BaseException(INVALID_STATE);
            }

            if (user_id == null && funding_id == null) {
                return fundingParticipantsRepository.findFundingParticipantsByState(fundingParticipantsState);
            } else if (user_id != null && funding_id == null) {
                // userid를 이용하여 user 객체 찾기
                User user = userRepository.findUserById(user_id)
                        .orElseThrow(() -> new BaseException(NOT_FIND_USER));

                return fundingParticipantsRepository.findFundingParticipantsByParticipantAndState(user, fundingParticipantsState);
            } else {
                // fundingId를 이용하여 funding 객체 가져오기
                Funding funding = fundingRepository.findFundingById(funding_id)
                        .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

                return fundingParticipantsRepository.findFundingParticipantsByFundingAndState(funding, fundingParticipantsState);
            }
        }

        //3. funding_id 가 null 이 아닌 경우
        if (funding_id != null) {
            // fundingId를 이용하여 funding 객체 가져오기
            Funding funding = fundingRepository.findFundingById(funding_id)
                    .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));

            if (user_id == null && state == null) {
                return fundingParticipantsRepository.findFundingParticipantsByFunding(funding);
            } else if (user_id != null && state == null) {
                // userid를 이용하여 user 객체 찾기
                User user = userRepository.findUserById(user_id)
                        .orElseThrow(() -> new BaseException(NOT_FIND_USER));

                FundingParticipants fundingParticipants = fundingParticipantsRepository.findFundingParticipantsByParticipantAndFunding(user, funding)
                        .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));
                return Collections.singletonList(fundingParticipants);
            } else {

                try {
                    Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(state.toUpperCase());

                    switch (fundingParticipantsState) {
                        case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                            return fundingParticipantsRepository.findFundingParticipantsByFundingAndState(funding, fundingParticipantsState);
                        }
                        default -> {
                            throw new BaseException(INVALID_STATE);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    throw new BaseException(INVALID_STATE);
                }
            }
        }
        return null;
    }





    // 펀딩 참여 리스트에 추가할 유저 id와 펀딩 id
    @Transactional
    public String JoinParticipants (Long user_id, Long funding_id) {
        // userid를 이용하여 user 객체 찾기
        User user = userRepository.findUserById(user_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(funding_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));


        // 객체 생성
        FundingParticipants fundingParticipants = new FundingParticipants(user, funding);

        // 참여자 객체 참여자 테이블에 저장
        fundingParticipantsRepository.save(fundingParticipants);

        return "참여자 입장 성공입니다. ";
    }



    // 펀딩 참여 리스트에 삭제할 유저 id와 펀딩 id
    @Transactional
    public String DeleteParticipants (Long user_id, Long funding_id) {
        // userid를 이용하여 user 객체 찾기
        User user = userRepository.findUserById(user_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(funding_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));


        // 참여자 객체 찾기
        FundingParticipants fundingParticipants = fundingParticipantsRepository.findFundingParticipantsByParticipantAndFunding(user, funding)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING_PARTICIPANTS));

        // 참여자 객체 참여자 테이블에서 삭제
        fundingParticipantsRepository.delete(fundingParticipants);

        return "참여자 삭제 성공입니다. ";
    }



    // 특정 펀딩 모집 성공 시 해당 펀딩 id를 가진 참여자 state를 ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE으로 변경
    /*<펀딩 참여자 테이블 상태값 목록>
        Funding_Participants 테이블의 상태(state):
        ACTIVE: 참여자가 펀딩 결제를 완료 상태
        COMPLETE: 신청한 펀딩 성공 상태
        REFUND_NEEDED: 신청한 펀딩이 실패하여 환불 필요 상태
        REFUND_COMPLETE: 환불 완료 상태*/

    @Transactional
    public String updateParticipantsState (Long user_id, Long funding_id, String forwardState) {
        // userid를 이용하여 user 객체 찾기
        User user = userRepository.findUserById(user_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // fundingId를 이용하여 user 객체 가져오기
        Funding funding = fundingRepository.findFundingById(funding_id)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING));


        // 참여자 객체 찾기
        FundingParticipants fundingParticipants = fundingParticipantsRepository.findFundingParticipantsByParticipantAndFunding(user, funding)
                .orElseThrow(() -> new BaseException(NOT_FIND_FUNDING_PARTICIPANTS));

        // String state가 상수값으로 등록된 상태값인지 확인
        try {
            Constant.FundingParticipantsState fundingParticipantsState = Constant.FundingParticipantsState.valueOf(forwardState.toUpperCase());

            // 참여자 객체의 state 변경 시도
            switch (fundingParticipantsState) {
                case ACTIVE, COMPLETE, REFUND_NEEDED, REFUND_COMPLETE -> {
                    fundingParticipants.updateFundingParticipantsState(fundingParticipantsState);
                }
                default -> {
                    throw new BaseException(INVALID_STATE);
                }
            }
        } catch (IllegalArgumentException e) {
            throw new BaseException(INVALID_STATE);
        }

        return "참여자 "+forwardState +" 으로 변경 성공입니다. ";
    }


    // 펀딩 마감 시간 확인 후 state 변경
    /*@Scheduled(cron = "0 0 0 * * ?") // 매일 24시 (자정)에 실행*/
    /*@Scheduled(cron = "0 0/30 * * * ?") // 매 30분마다 실행*/
    @Scheduled(cron = "0 * * * * ?") // 매 분마다 실행
    public void updateFundingState() {
        // 현재 진행중인 펀딩 게시글 추출
        List<Funding> fundingList = OrderByState(getAllFundings(), "IN_PROGRESS");

        for (Funding funding : fundingList){
            // 펀딩 달성률
            int percentage = calculateAchievementRate(funding.getId());

            LocalDateTime currentTime = LocalDateTime.now();
            LocalDateTime deadLine = funding.getDeadline();

            // 마감일이 지났을 경우,
            if (deadLine.isBefore(currentTime) && percentage < 100) {
                log.info("마감일 지남-> state FAIL 변경");
                funding.updateState(Constant.FundingState.FAIL);
            } else if (deadLine.isBefore(currentTime) && percentage >= 100) {
                funding.updateState(Constant.FundingState.COMPLETE);
                log.info("마감일 지남-> state COMPLETE 변경");
            }
        }
    }



    @Transactional(readOnly = true)
    public FundingPayment getPaymentInfoByLoginUser (HttpServletRequest request) {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태 예외 처리
        else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // 유저의 결제 정보 찾기
        Optional<FundingPayment> optionalFundingPayment = fundingPaymentRepository.findFundingPaymentByParticipant(user);

        // 유저의 결제 정보가 등록 되어있을 경우,
        if (optionalFundingPayment.isPresent()) {
            return optionalFundingPayment.get();
        }

        // 유저의 결제 정보가 없을 경우
        else {
            throw new BaseException(EMPTY_PAYMENT_USER);
        }
    }



    @Transactional
    public FundingPayment InsertPaymentInfoByLoginUser (HttpServletRequest request, PaymentInsertReq paymentInsertReq) throws SQLException {
        // 브라우저에서 jwt 토큰 값 찾기
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }
        // 브라우저에 토큰값이 없으면, 로그아웃 상태 예외 처리
        else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new BaseException(NOT_FIND_USER));

        // 유저의 결제 정보 찾기
        Optional<FundingPayment> optionalFundingPayment = fundingPaymentRepository.findFundingPaymentByParticipant(user);

        // 유저의 결제 정보가 등록 되어있을 경우, 이미 있는 유저임을 반환
        if (optionalFundingPayment.isPresent()) {
            throw new BaseException(EXISTS_PAYMENT_USER);
        }

        // 유저의 결제 정보가 없을 경우, Payment 테이블에 정보 저장
        else {
            // 은행 이름 및 은행 계좌 저장.
            FundingPayment fundingPayment = paymentInsertReq.toEntity();

            // 유저 정보 저장
            fundingPayment.updateParticipants(user);

            // DB에 fundingPayment 객체 저장
            fundingPaymentRepository.save(fundingPayment);

            return fundingPayment;
        }
    }
}
