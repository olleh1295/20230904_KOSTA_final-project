package com.example.final_project.src.funding.model;

import com.example.final_project.common.Constant;
import com.example.final_project.src.funding.entity.Funding;
import com.example.final_project.src.funding.entity.FundingComment;
import com.example.final_project.src.user.entity.User;
import lombok.*;

import java.sql.SQLException;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommentInsertReq {
    private Long funding_id;
    private String content;

/*
    public CommentInsertReq(String content) {
        this.content = content;
    }
*/

    public FundingComment toEntity() {
        return FundingComment.builder()
                .content(this.content)
                .build();
    }


}
