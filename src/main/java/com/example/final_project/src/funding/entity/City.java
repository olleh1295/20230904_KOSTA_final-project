package com.example.final_project.src.funding.entity;

import com.example.final_project.common.entity.BaseEntity;
import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(callSuper = false)
@Getter
@Entity // 필수, Class 를 Database Table화 해주는 것이다
@EntityScan
@Table(name = "City")// Table 이름을 명시해주지 않으면 class 이름을 Table 이름으로 대체한다.)
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "city_sequence")
    @SequenceGenerator(name = "city_sequence", sequenceName = "CITY_SEQUENCE", allocationSize = 1)
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    private City parent; // 부모 시/군구

    @Builder
    public City(String name, City parent) {
        this.name = name;
        this.parent = parent;
    }


    // 생성 예시

    /*// 서울특별시 생성
    City seoul = City.builder()
            .name("서울특별시")
            .build();

    // 관악구 생성 및 서울특별시를 부모로 설정
    City guro = City.builder()
            .name("관악구")
            .parent(seoul)
            .build();*/

}
