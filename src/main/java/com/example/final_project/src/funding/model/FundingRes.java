package com.example.final_project.src.funding.model;

import com.example.final_project.src.funding.entity.Funding;
import com.example.final_project.src.funding.entity.FundingCategory;
import com.example.final_project.src.user.entity.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FundingRes {
    private Long id;
    private String title;
    private String subtitle;
    private FundingCategory fundingCategory;
    private User writer;
    private String deadline;
    private Long likeCount;
    private Long viewCount;
    private String thumbnail;
    private String content;
    @JsonProperty("liked") // JSON 필드 이름을 "liked"로 설정
    private Boolean liked;
    private int achievementrate;

    public FundingRes(Funding funding, Boolean liked, FundingCategory fundingCategory, User writer, int achievementrate) {
        this.id = funding.getId();
        this.title = funding.getTitle();
        this.subtitle = funding.getSubtitle();
        this.fundingCategory = fundingCategory;
        this.writer = writer;
        this.deadline = LocalDateTimeToString(funding.getDeadline());
        this.likeCount = funding.getLikeCount();
        this.viewCount = funding.getViewCount();
        this.liked = liked;
        this.thumbnail = funding.getThumbnail();
        this.content = funding.getContent();
        this.achievementrate = achievementrate;
    }


    public String LocalDateTimeToString (LocalDateTime deadline) {
        // Create a DateTimeFormatter with the desired format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy년 MM월 dd일, HH시 mm분");

        // Format the LocalDateTime instance to a string
        String formattedDeadline = deadline.format(formatter);

        // Print the formatted date
        return formattedDeadline;
    }
}
