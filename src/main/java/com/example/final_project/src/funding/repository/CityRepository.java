package com.example.final_project.src.funding.repository;

import com.example.final_project.src.funding.entity.City;
import com.example.final_project.src.funding.entity.Funding;
import com.example.final_project.src.funding.entity.FundingComment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Long> {
    // 대한민국 시 리스트
    List<City> findCitiesByParentNull();

    // 대한민국 군, 구 리스트
    List<City> findCitiesByParentNotNull();

    // 시를 입력받아 시에 맞는 군, 구 리스트 출력
    List<City> findCitiesByParentContaining(String cityName);
    Optional<City> findCitiesByParentId(Long parentId);
}
