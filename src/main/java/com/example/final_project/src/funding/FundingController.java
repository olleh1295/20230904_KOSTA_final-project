package com.example.final_project.src.funding;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.src.funding.entity.*;
import com.example.final_project.src.funding.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.final_project.common.response.BaseResponseStatus.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class FundingController {
    @Autowired
    private FundingService fundingService;


    /**
     * 필터링 및 정렬을 적용한 펀딩 게시글 목록 조회 API
     * [GET] /funding/all
     *
     * @param category_id (optional) 대분류: 카테고리
     * @param condition   (optional)   중분류: 최신순 (newest) --> default / 마감임박 순 (deadline) / 좋아요 순 (인기순, mostLike)
     * @param page_num    소분류: 현재 페이지 숫자 ---> default 모든 페이지 출력
     * @param itemsPerPage 한 페이지에 출력할 아이템 수
     * @return BaseResponse<List < FundingRes>>
     */
    @ResponseBody
    @GetMapping("/funding/all")
    public BaseResponse<List<FundingRes>> readAllFundingPosts(HttpServletRequest request,
                                                              @RequestParam(name = "category", required = false) Long category_id,
                                                              @RequestParam(name = "orderby", required = false) String condition,
                                                              @RequestParam(name = "state", required = false) String state,
                                                              @RequestParam(name = "currentpage", required = false) Integer page_num,
                                                              @RequestParam(name = "itemsPerPage", required = false) Integer itemsPerPage) {

        // 1. 카테고리 분류 결과
        List<Funding> fundingList1 = null;
        if (category_id == null || category_id < 1) {
            fundingList1 = fundingService.getAllFundings();
        } else {
            fundingList1 = fundingService.getFundingsByCategory(category_id);
        }

        // 2.
        //  펀딩 게시글 리스트 order by 1. 최신순 (newest) --> default
        //	펀딩 게시글 리스트 order by 2. 마감임박 순 (deadline)
        //	펀딩 게시글 리스트 order by 3. 좋아요 순 (인기순, mostLike)

        List<Funding> fundingList2 = fundingService.OrderByKeyword(fundingList1, condition); //

        // 3. state 분류 결과
        List<Funding> fundingList3 = fundingService.OrderByState(fundingList2, state);

        // 4. 페이지네이션
        List<Funding> fundingList4 = null;

        // 페이지 숫자 null이면 모든 키워드 리스트 반환
        if ((page_num == null && itemsPerPage == null)) {
            fundingList4 = fundingList3;
        }
        else if (page_num == null || itemsPerPage == null) {
            throw new BaseException(EMPTY_PAGE_NUM);
        }
        // page_num 값이 정상이면 한 페이지 아이템 리스트로 반환
        else{
            fundingList4 = fundingService.getFundingsBypage(fundingList3, page_num, itemsPerPage);
        }

        // 4. 현재 로그인한 유저의 게시글 좋아요 여부 및 펀딩 게시글 달성률 추가 --> FundingRes 모델
        List<Boolean> booleanList = fundingService.getLikedFundingList(request, fundingList4);

        List<Funding> finalFundingList = fundingList4;
        List<FundingRes> responseList = IntStream.range(0, fundingList4.size())
                .mapToObj(i -> {
                    Funding funding = finalFundingList.get(i);
                    Boolean liked = booleanList.get(i);
                    int achievementrate = fundingService.calculateAchievementRate(funding.getId());
                    return new FundingRes(funding, liked, funding.getFundingCategory(), funding.getWriter(), achievementrate);
                })
                .collect(Collectors.toList());

        return new BaseResponse<>(responseList);
    }


    /**
     * 특정 유저 혹은 특정 조건의 펀딩 게시글 목록 조회 API
     * [GET] /fundingList
     *
     * @param user_id (optional) 조회할 유저의 ID
     * @param keyword (optional) 검색할 키워드
     * @param page_num    소분류: 현재 페이지 숫자 ---> default 모든 페이지 출력
     * @param itemsPerPage 한 페이지에 출력할 아이템 수
     * @param funding_id (optional) 조회할 펀딩 ID
     * @return BaseResponse<List < Funding>>
     */
    @ResponseBody
    @GetMapping("/fundingList")
    public BaseResponse<List<Funding>> readFundingPosts(
            @RequestParam(name = "keyword", required = false) String keyword,
            @RequestParam(name = "currentpage", required = false) Integer page_num,
            @RequestParam(name = "itemsPerPage", required = false) Integer itemsPerPage,
            @RequestParam(name = "user", required = false) Long user_id,
            @RequestParam(name = "funding", required = false) Long funding_id) {

        if (keyword == null || keyword.isEmpty()){
            // user_id 사용하여 특정 조건의 게시글 페이지를 가져오기
            if (funding_id == null || funding_id < 1) {
                // user_id와 funding_id가 모두 null 이면 예외처리
                if (user_id == null || user_id < 1 ) {
                    throw new BaseException(ALL_EMPTY_CONDITION);
                }
                // user_id를 사용하여 해당 유저의 펀딩 게시글을 가져오기
                else {
                    return new BaseResponse<>(fundingService.getFundingsByUser(user_id));
                }
            }

            // funding_id를 사용하여 특정 조건의 게시글 페이지를 가져오기
            else {
                Funding funding = fundingService.getFundingByFunding(funding_id);
                if (funding != null) {
                    return new BaseResponse<>(Collections.singletonList(funding));
                }
            }
        }

        // 헤더바 검색창으로 검색하여 찾는 경우 -> keyword가 있을 경우 입력한 키워드가 title, subtitle, category에서 부분 일치하는 게시글 조회
        else {
            // 페이지 숫자 유효성 실패로 모든 키워드 리스트 반환
            if ((page_num == null && itemsPerPage == null)) {
                return new BaseResponse<>(fundingService.getFundingsByKeyword(keyword));
            }
            else if (page_num == null || itemsPerPage == null) {
                throw new BaseException(EMPTY_PAGE_NUM);
            }
            else{
                // 페이지네이션 키워드 리스트 반환
                List<Funding> fundingList5 = fundingService.getFundingsByKeyword(keyword);
                return new BaseResponse<>(fundingService.getFundingsBypage(fundingList5, page_num, itemsPerPage));
            }
        }

        return null;
    }

    /**
     * 좋아요 버튼 클릭 반응으로 PostLike 테이블에 저장할 API
     * [GET] /funding/toggle-like
     *
     * @param request    HTTP 요청 객체
     * @param funding_id 게시글 ID
     * @return BaseResponse<String>
     */
    @ResponseBody
    @GetMapping("/funding/toggle-like")
    public BaseResponse<String> addLikeUser(HttpServletRequest request, @RequestParam(name = "id") Long funding_id) {
        return new BaseResponse<>(fundingService.updatePostLike(request, funding_id));
    }

    /**
     * 특정 펀딩 게시글의 조회수, 좋아요수, 댓글수 조회 API
     * [GET] /funding/total
     *
     * @param funding_id 게시글 ID
     * @return BaseResponse<Map < String, Long>>
     */
    @ResponseBody
    @GetMapping("/funding/total")
    public BaseResponse<Map<String, Long>> ViewLikeComment(@RequestParam(name = "id") Long funding_id) {
        // funding id를 이용하여 해당 펀딩의 조회수 가져오기
        Long viewCount = fundingService.countFundingView(funding_id);

        // funding id를 이용하여 해당 펀딩의 좋아요 수 가져오기
        Long likeCount = fundingService.countFundingLike(funding_id);

        // funding id를 이용하여 해당 펀딩의 댓글수 가져오기
        Long commentCount = fundingService.countFundingComment(funding_id);

        Map<String, Long> totalCount = new HashMap<>();
        totalCount.put("view", viewCount);
        totalCount.put("like", likeCount);
        totalCount.put("comment", commentCount);
        return new BaseResponse<>(totalCount);
    }


    /**
     * 모든 펀딩 게시글을 조건대로 분류하여 리스트 반환 API
     * [GET] /funding/sort
     *
     * @param status  (optional) 펀딩 상태 선택
     * @param geo_num (optional) 지역 선택
     * @return BaseResponse<List < Funding>>
     */
    @ResponseBody
    @GetMapping("/funding/sort")
    public BaseResponse<List<Funding>> SortedByKeyword(
            @RequestParam(name = "status", required = false) String status,
            @RequestParam(name = "geo", required = false) Long geo_num) {
        // 펀딩 상태에 따른 분류 혹은 지역 넘버에 따른 분류 실행
        if (status != null || geo_num != null) {
            /*public enum FundingState {
                ACTIVE, IN_PROGRESS, DELETE, COMPLETE, FAIL;
            }*/
            List<Funding> sortedFundingList = fundingService.SortedByKeyword(status, geo_num);
            return new BaseResponse<>(sortedFundingList);
        }

        // 모든 분류 조건이 null 이면 모든 게시물 조회 (default) 동작 수행
        else {
            return readFundingPosts(null, null, null, null, null);
        }
    }


    /*    *//**
     * 모든 펀딩 게시글을 조건대로 정렬하여 리스트 반환 API
     * [GET] /funding/order
     * @param condition (optional) 최신순(default) / 마감임박순 / 좋아요순 선택
     * @return BaseResponse<List < Funding>>
     *//*
    @ResponseBody
    @GetMapping("/funding/order")
    public BaseResponse<List<Funding>> OrderByKeyword(
            @RequestParam(name = "orderby", required = false) String condition,
            @RequestParam(name = "currentpage", required = false) Integer page_num) {

        int itemsPerPage = 12; // 한 페이지당 표시할 아이템 수
        List<Funding> fundingList;

        if (page_num == null) {
            fundingList = fundingService.OrderByKeyword(condition); // 조건에 맞는 펀딩글 리스트
        } else {
            fundingList = fundingService.getFundingsBypage(fundingService.OrderByKeyword(condition), page_num, itemsPerPage); // 페이지네이션 적용
        }

        return new BaseResponse<>(fundingList);
    }*/

    /**
     * 펀딩 게시글 id를 받아 해당 펀딩의 달성률 반환 API, 소수점없이 일의 자리 int형으로 반환
     * [GET] /funding/achievementrate
     *
     * @param funding_id 펀딩 고유 id
     * @return BaseResponse<int>
     */
    @ResponseBody
    @GetMapping("/funding/achievementrate")
    public BaseResponse<Integer> getFundingByFunding(@RequestParam(name = "funding") Long funding_id) {
        return new BaseResponse<>(fundingService.calculateAchievementRate(funding_id));
    }


    /**
     * 펀딩 게시글 생성 API
     * [POST] /funding/create
     *
     * @param request
     * @param fundingInsertReq 게시글 생성 모델
     * @return BaseResponse<FundingInsertRes>
     */
    @ResponseBody
    @PostMapping("/funding/create")
    public BaseResponse<FundingInsertRes> createPost(HttpServletRequest request, @RequestBody FundingInsertReq fundingInsertReq) throws SQLException {
       /* log.info("게시글 작성 컨트롤러 진입, getTitle= {}", fundingInsertReq.getTitle());
        log.info("게시글 작성 컨트롤러 진입, getSubtitle= {}", fundingInsertReq.getSubtitle());
        log.info("게시글 작성 컨트롤러 진입, getCategoryId= {}", fundingInsertReq.getCategory_id());
        log.info("게시글 작성 컨트롤러 진입, getWriter= {}", fundingInsertReq.getWriter());
        log.info("게시글 작성 컨트롤러 진입, getLocation= {}", fundingInsertReq.getLocation());
        log.info("게시글 작성 컨트롤러 진입, getDeadline= {}", fundingInsertReq.getDeadline());*/
        log.info("게시글 작성 컨트롤러 진입, getDeadline= {}", fundingInsertReq.toString());
        if (validatePostFormReq(fundingInsertReq) != null) {
            return validatePostFormReq(fundingInsertReq);
        } else {
            return new BaseResponse<>(fundingService.createFunding(request, fundingInsertReq));
        }
    }

    // 게시글 정보 데이터 유효성 확인
    private BaseResponse<FundingInsertRes> validatePostFormReq(FundingInsertReq fundingInsertReq) {
        if (fundingInsertReq.getTitle() == null || fundingInsertReq.getSubtitle() == null
                || fundingInsertReq.getDeadline() == null) {
            // 제목, 부제목., 마감일 공란일 때 예외 처리
            throw new BaseException(FUNDING_EMPTY_TITLE);
        }
        if (fundingInsertReq.getGoal() == null || fundingInsertReq.getMin_participants() == null
                || fundingInsertReq.getMax_participants() == null) {
            // 목표인원, 최소인원, 최대인원 공란일 때 예외 처리
            throw new BaseException(FUNDING_EMPTY_PARTICIPANT);
        }

        if (fundingInsertReq.getAmount() == null) {
            // 펀딩 금액 공란일 때 예외 처리
            throw new BaseException(FUNDING_EMPTY_AMOUNT);
        }

        if (fundingInsertReq.getThumbnail() == null || fundingInsertReq.getContent() == null) {
            // 썸네일, 콘텐츠가 공란일 때 예외처리
            throw new BaseException(FUNDING_EMPTY_BLOB);
        }

        return null; // 유효성 검사 통과
    }


    /**
     * 모든 카테고리 리스트 반환 API
     * [GET] /category/all
     *
     * @return BaseResponse<List < FundingCategory>>
     */
    @ResponseBody
    @GetMapping("/category/all")
    public BaseResponse<List<FundingCategory>> readAllFundingCategories() {
        return new BaseResponse<>(fundingService.getCategoryAllList());
    }


    /**
     * 특정 카테고리 id 리스트 반환 API
     * [GET] /category
     *
     * @param category_id 카테고리 id
     * @return BaseResponse<List < FundingCategory>>
     */
    @ResponseBody
    @GetMapping("/category")
    public BaseResponse<FundingCategory> readFundingCategories(@RequestParam(name = "id") Long category_id) {
        return new BaseResponse<>(fundingService.getCategoryById(category_id));
    }


    /**
     * 특정 유저의 댓글 리스트 반환 API
     * [GET] /fundingcomment/user
     *
     * @param request HttpServletRequest
     * @return BaseResponse<List < FundingComment>>
     */
    @ResponseBody
    @GetMapping("/fundingcomment/user")
    public BaseResponse<List<FundingComment>> readUserComments(HttpServletRequest request) {
        return new BaseResponse<>(fundingService.getCommentByUser(request));
    }


    /**
     * 특정 게시글의 모든 댓글 리스트 반환 API
     * [GET] /fundingcomment
     *
     * @param funding_id 펀딩 게시르 고유 id
     * @return BaseResponse<List < FundingComment>>
     */
    @ResponseBody
    @GetMapping("/fundingcomment")
    public BaseResponse<List<FundingComment>> readFundingComments(@RequestParam(name = "funding") Long funding_id) {
        return new BaseResponse<>(fundingService.getCommentByFunding(funding_id));
    }


    /**
     * 게시글 댓글 작성 API
     * [POST] /fundingcomment/create
     *
     * @param request          HttpServletRequest
     * @param commentInsertReq 댓글 생성 모델
     * @return BaseResponse<String>
     */
    @ResponseBody
    @PostMapping("/fundingcomment/create")
    public BaseResponse<String> createFundingComments(HttpServletRequest request, @RequestBody CommentInsertReq commentInsertReq) {
        return new BaseResponse<>(fundingService.insertCommentByUser(request, commentInsertReq));
    }


    /**
     * 로그인한 유저가 참여중인 펀딩 리스트 반환 API
     * [GET] /participants/currentuser
     *
     * @param request    HttpServletRequest
     * @param state      (Optional) 참여자 상태
     * @param funding_id (Optional) 게시글 고유 id
     * @return BaseResponse<List < FundingParticipants>>
     */
    @ResponseBody
    @GetMapping("/participants/currentuser")
    public BaseResponse<List<FundingParticipants>> findParitipantsByLoginUser(HttpServletRequest request,
                                                                              @RequestParam(name = "state", required = false) String state,
                                                                              @RequestParam(name = "funding", required = false) Long funding_id) {
        /*<펀딩 참여자 테이블 상태값 목록>
        Funding_Participants 테이블의 상태(state):
        ACTIVE: 참여자가 펀딩 결제를 완료 상태
        COMPLETE: 신청한 펀딩 성공 상태
        REFUND_NEEDED: 신청한 펀딩이 실패하여 환불 필요 상태
        REFUND_COMPLETE: 환불 완료 상태*/

        return new BaseResponse<>(fundingService.getParticipantsByLoginUserAndStateAndFunding(request, state, funding_id));
    }


    /**
     * 한 유저가 참여중인 펀딩 리스트 반환 API
     * [GET] /participants/user
     *
     * @param user_id    (Optional) 유저 고유 id
     * @param state      (Optional) 펀딩 참여자 상태
     * @param funding_id (Optional) 게시글 고유 id
     * @return BaseResponse<List < FundingParticipants>>
     */
    @ResponseBody
    @GetMapping("/participants/user")
    public BaseResponse<List<FundingParticipants>> findParitipantsByCondition(@RequestParam(name = "user", required = false) Long user_id,
                                                                              @RequestParam(name = "state", required = false) String state,
                                                                              @RequestParam(name = "funding", required = false) Long funding_id) {
        /*<펀딩 참여자 테이블 상태값 목록>
        Funding_Participants 테이블의 상태(state):
        ACTIVE: 참여자가 펀딩 결제를 완료 상태
        COMPLETE: 신청한 펀딩 성공 상태
        REFUND_NEEDED: 신청한 펀딩이 실패하여 환불 필요 상태
        REFUND_COMPLETE: 환불 완료 상태*/

        if (user_id == null && state == null && funding_id == null) {
            throw new BaseException(ALL_EMPTY_CONDITION);
        } else {
            return new BaseResponse<>(fundingService.getParticipantsByUserAndStateAndFunding(user_id, state, funding_id));
        }
    }


    /**
     * 한 유저의 참여 펀딩 리스트 state 변경 API
     * [GET] /participants/{forwardState}
     *
     * @param user_id    유저 고유 id
     * @param funding_id 게시글 고유 id
     * @return BaseResponse<List < FundingParticipants>>
     */
    @ResponseBody
    @GetMapping("/participants/{forwardState}")
    public BaseResponse<String> updatePartipantsState(@PathVariable(name = "forwardState") String forwardState,
                                                      @RequestParam(name = "id") Long user_id,
                                                      @RequestParam(name = "funding") Long funding_id) {
        switch (forwardState) {
            // 펀딩 참여 리스트에 추가할 유저 id와 펀딩 id
            case "join" -> {
                return new BaseResponse<>(fundingService.JoinParticipants(user_id, funding_id));
            }
            // 펀딩 참여 리스트에 삭제할 유저 id와 펀딩 id
            case "delete" -> {
                return new BaseResponse<>(fundingService.DeleteParticipants(user_id, funding_id));
            }
            // 특정 펀딩 모집 성공 시 해당 펀딩 id를 가진 참여자 state를 COMPLETE으로 변경
            case "complete" -> {
                return new BaseResponse<>(fundingService.updateParticipantsState(user_id, funding_id, "COMPLETE"));
            }
            // 특정 펀딩 모집 실패 시 해당 펀딩 id를 가진 참여자 state를 REFUND_NEEDED으로 변경
            case "fail" -> {
                return new BaseResponse<>(fundingService.updateParticipantsState(user_id, funding_id, "REFUND_NEEDED"));
            }
            // 펀딩 환불이 완료되면 참여자 state를 REFUND_COMPLETE으로 변경
            case "refund" -> {
                return new BaseResponse<>(fundingService.updateParticipantsState(user_id, funding_id, "REFUND_COMPLETE"));
            }
            default -> {
                throw new BaseException(INVALID_STATE);
            }
        }
    }


    /**
     * 로그인한 유저의 결제 정보 출력 API
     * [GET] /payment/find
     *
     * @param request HttpServletRequest
     * @return BaseResponse<FundingPayment>
     */
    @ResponseBody
    @GetMapping("/payment/find")
    public BaseResponse<FundingPayment> findPaymentInfoByUser(HttpServletRequest request) {

        return new BaseResponse<>(fundingService.getPaymentInfoByLoginUser(request));
    }


    /**
     * 유저의 결제 정보 생성 API
     * [POST] /payment/create
     *
     * @param request          HttpServletRequest
     * @param paymentInsertReq 유저 결제 정보 입력 모델
     * @return BaseResponse<FundingPayment>
     */
    @ResponseBody
    @PostMapping("/payment/create")
    public BaseResponse<FundingPayment> createPaymentInfo(HttpServletRequest request, @RequestBody PaymentInsertReq paymentInsertReq) throws SQLException {

        if (validatePaymentFormReq(paymentInsertReq) != null) {
            return validatePaymentFormReq(paymentInsertReq);
        } else {
            return new BaseResponse<>(fundingService.InsertPaymentInfoByLoginUser(request, paymentInsertReq));
        }

    }

    // 유저 결제 정보 데이터 유효성 확인
    private BaseResponse<FundingPayment> validatePaymentFormReq(PaymentInsertReq paymentInsertReq) {
        if (paymentInsertReq.getBank_name() == null) {
            // 은행 이름 공란일때
            throw new BaseException(EMPTY_PAYMENT_BANK);
        }
        if (paymentInsertReq.getBank_account() == null) {
            // 은행 이름 공란일때
            throw new BaseException(EMPTY_PAYMENT_BANK_ACCOUNT);
        }
        return null; // 유효성 검사 통과
    }


}