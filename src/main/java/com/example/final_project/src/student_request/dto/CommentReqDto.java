package com.example.final_project.src.student_request.dto;

import com.example.final_project.src.student_request.entity.StudentComment;
import com.example.final_project.src.student_request.entity.StudentRequest;
import com.example.final_project.src.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentReqDto {

    private Long student_request_id;
    private String content;

    public StudentComment toEntity(){
        return StudentComment.builder()
                .content(this.content)
                .build();
    }

}
