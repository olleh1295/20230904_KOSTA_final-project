package com.example.final_project.src.student_request;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.jwt.JwtUtil;
import com.example.final_project.common.response.BaseResponseStatus;
import com.example.final_project.src.student_request.dto.CommentReqDto;
import com.example.final_project.src.student_request.dto.CommentResDto;
import com.example.final_project.src.student_request.entity.StudentComment;
import com.example.final_project.src.student_request.entity.StudentRequest;
import com.example.final_project.src.user.UserRepository;
import com.example.final_project.src.user.entity.User;
import com.example.final_project.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.final_project.common.response.BaseResponseStatus.*;

@RequiredArgsConstructor
@Service
@Slf4j
@Transactional
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtil jwtUtil;

    @Transactional
    public String insertStudentComment(HttpServletRequest request, CommentReqDto commentReqDto){
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        }else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username).orElseThrow(() ->
                new BaseException(NOT_FIND_USER));
        Long student_request_id = commentReqDto.getStudent_request_id();
        StudentRequest studentRequest = studentRepository.findStudentRequestById(student_request_id).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST));

        StudentComment studentComment = commentReqDto.toEntity();
        studentComment.updateStudentRequest(studentRequest);
        studentComment.updateWriter(user);
        commentRepository.save(studentComment);

        return "댓글이 입력되었습니다.";
    }

    @Transactional
    public String updateStudentComment(Long user_id, Long student_request_id, CommentReqDto commentReqDto){
        User user = userRepository.findUserById(user_id).orElseThrow(() ->
                new BaseException(NOT_FIND_USER));

        StudentRequest studentRequest = studentRepository.findStudentRequestById(student_request_id).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST));

        StudentComment studentComment = commentRepository.findStudentCommentByWriterAndStudentRequest(user, studentRequest).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST_COMMENT));

        studentComment.updateStudentComment(commentReqDto.getContent());
        commentRepository.save(studentComment);

        return "댓글이 수정되었습니다.";
    }

    @Transactional
    public String deleteStudentComment(Long user_id, Long student_request_id){
        User user = userRepository.findUserById(user_id).orElseThrow(() ->
                new BaseException(NOT_FIND_USER));

        StudentRequest studentRequest = studentRepository.findStudentRequestById(student_request_id).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST));

        StudentComment studentComment = commentRepository.findStudentCommentByWriterAndStudentRequest(user,studentRequest).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST_COMMENT));

        commentRepository.delete(studentComment);

        return "댓글을 삭제했습니다.";
    }

    //댓글 리스트
    @Transactional(readOnly = true)
    public List<StudentComment> getStudentCommentByUser (HttpServletRequest request) {
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if(jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        } else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username).orElseThrow(() ->
                new BaseException(NOT_FIND_USER));

        return commentRepository.findStudentCommentByWriter(user);
    }

    @Transactional(readOnly = true)
    public List<StudentComment> getStudentCommentByStudentRequest(Long studentRequestId){
        StudentRequest studentRequest = studentRepository.findStudentRequestById(studentRequestId).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST));

        return commentRepository.findStudentCommentByStudentRequest(studentRequest);
    }

}
