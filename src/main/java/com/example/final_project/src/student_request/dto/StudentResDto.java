package com.example.final_project.src.student_request.dto;

import com.example.final_project.src.funding.entity.FundingCategory;
import com.example.final_project.src.student_request.entity.StudentRequest;
import com.example.final_project.src.user.entity.User;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StudentResDto {

    private Long id;
    private String title;
    private FundingCategory fundingCategory;
    private User writer;
    private String content;
    private Long likeCount;
    private Long viewCount;


    public StudentResDto(StudentRequest studentRequest, FundingCategory fundingCategory, User writer) {
        this.id = studentRequest.getId();
        this.title = studentRequest.getTitle();
        this.fundingCategory = fundingCategory;
        this.writer = writer;
        this.content = studentRequest.getContent();
        this.likeCount = studentRequest.getLikeCount();
        this.viewCount = studentRequest.getViewCount();

    }
}
