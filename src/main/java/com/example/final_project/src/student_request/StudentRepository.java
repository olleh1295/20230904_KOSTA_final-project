package com.example.final_project.src.student_request;

import com.example.final_project.src.funding.entity.FundingCategory;
import com.example.final_project.src.student_request.entity.StudentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<StudentRequest, Long> {

    //전체 게시글 리스트
    List<StudentRequest> findAllBy();

    //특정 카테고리 Id를 가진 게시글 리스트
    Optional<StudentRequest> findStudentRequestByFundingCategory(Long id);

    //좋아요 순으로 게시글 내림차순 리스트
    List<StudentRequest> findByOrderByLikeCountDesc();

    //조회수 순으로 게시글 내림차순 리스트
    List<StudentRequest> findByOrderByViewCountDesc();

    Optional<StudentRequest> findStudentRequestById(Long id);

}
