package com.example.final_project.src.student_request.controller;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.common.response.BaseResponseStatus;
import com.example.final_project.src.funding.entity.FundingCategory;
import com.example.final_project.src.student_request.StudentService;
import com.example.final_project.src.student_request.dto.StudentReqDto;
import com.example.final_project.src.student_request.dto.StudentResDto;
import com.example.final_project.src.student_request.dto.StudentcreateResDto;
import com.example.final_project.src.student_request.entity.StudentRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.final_project.common.response.BaseResponseStatus.*;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class StudentRequestController {

    @Autowired
    private StudentService studentService;


    //전체 게시글 리스트 조회
    @ResponseBody
    @GetMapping("/studentrequest/all")
    public BaseResponse<List<StudentResDto>> getallStudentRequestPosts(HttpServletRequest request){
        List<StudentRequest> studentRequestList = studentService.getAllStudentRequest();
        List<StudentResDto> studentResDtoList = new ArrayList<>();

        for(int i=0; i<studentRequestList.size(); i++) {
            StudentRequest studentRequest = studentRequestList.get(i);
            StudentResDto studentResDto = new StudentResDto(studentRequest, studentRequest.getFundingCategory(), studentRequest.getWriter());
            studentResDtoList.add(studentResDto);
        }

        return new BaseResponse<>(studentResDtoList);
    }

    //게시글 조회수, 좋아요수 조회
    @ResponseBody
    @GetMapping("/studentrequest/count")
    public BaseResponse<Map<String, Long>> ViewLikeCount(@RequestParam(name = "id") Long student_request_id) {
        Long viewCount = studentService.updateViewCount(student_request_id);
        Long likeCount = studentService.updateLikeCount(student_request_id);

        Map<String, Long> count = new HashMap<>();
        count.put("view", viewCount);
        count.put("like", likeCount);
        return new BaseResponse<>(count);
    }

    //조회수, 좋아요수 순으로 리스트 조회
    @ResponseBody
    @GetMapping("/studentrequest/lineup")
    public BaseResponse<List<StudentRequest>> OrderByKeyword(@RequestParam(name = "orderby", required = false) String condition) {
        return new BaseResponse<>(studentService.OrderByKeyword(condition));
    }

    //게시글 작성
    @ResponseBody
    @PostMapping("/studentrequest/save")
    public BaseResponse<StudentcreateResDto> savePosts(HttpServletRequest request, @RequestBody StudentReqDto studentReqDto) throws SQLException {
        log.info("게시글 작성, getTitle={}", studentReqDto.getTitle());
        log.info("게시글 작성, getCategoryId={}", studentReqDto.getCategory_id());
        log.info("게시글 작성, getWriter={}", studentReqDto.getWriter());
        log.info("게시글 작성, getContent={}", studentReqDto.getContent());

        if (validStudentRequestPosts(studentReqDto)!= null){
            return validStudentRequestPosts(studentReqDto);
        } else {
            return new BaseResponse<>(studentService.createStudentRequest(request, studentReqDto));
        }
    }

    //유효성 검사
    private BaseResponse<StudentcreateResDto> validStudentRequestPosts(StudentReqDto studentReqDto) {
        if (studentReqDto.getTitle() == null) {
            //제목 공란일 때 예회 처리
            throw new BaseException(STUDENTREQUEST_EMPTY_TITLE);
        }
        if (studentReqDto.getCategory_id() == null) {
            //카테고리 공란일 때 예외 처리
            throw new BaseException(STUDENTREQUEST_EMPTY_CATEGORY);
        }
        if(studentReqDto.getContent() == null) {
            //콘텐츠가 공란일 때 에외처리
            throw new BaseException(STUDENTREQUEST_EMPTY_BLOB);
        }

        return null;
    }

    //카테고리 리스트
    @ResponseBody
    @GetMapping("/postcategory/all")
    public BaseResponse<List<FundingCategory>> getAllStudentReuquestCategory() {
        return new BaseResponse<>(studentService.getStudentRequestCategoryAllList());
    }
    @ResponseBody
    @GetMapping("/postcategory")
    public BaseResponse<FundingCategory> getStudentRequestCategory(@RequestParam(name = "id") Long category_id) {
        return new BaseResponse<>(studentService.getStudentRequestCategoryById(category_id));
    }


}
