package com.example.final_project.src.student_request;

import com.example.final_project.common.Constant;
import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.jwt.JwtUtil;
import com.example.final_project.common.response.BaseResponseStatus;
import com.example.final_project.src.funding.entity.FundingCategory;
import com.example.final_project.src.funding.repository.FundingCategoryRepository;
import com.example.final_project.src.post_like.PostLikeRepository;
import com.example.final_project.src.post_like.entity.PostLike;
import com.example.final_project.src.student_request.dto.StudentReqDto;
import com.example.final_project.src.student_request.dto.StudentResDto;
import com.example.final_project.src.student_request.dto.StudentcreateResDto;
import com.example.final_project.src.student_request.entity.StudentComment;
import com.example.final_project.src.student_request.entity.StudentRequest;
import com.example.final_project.src.user.UserRepository;
import com.example.final_project.src.user.entity.User;
import com.example.final_project.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nonapi.io.github.classgraph.utils.VersionFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.final_project.common.response.BaseResponseStatus.*;

@Slf4j
@RequiredArgsConstructor
@Service
@Transactional
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostLikeRepository postLikeRepository;
    @Autowired
    private FundingCategoryRepository fundingCategoryRepository;
    @Autowired
    private JwtUtil jwtUtil;


    @Transactional
    public StudentcreateResDto createStudentRequest(HttpServletRequest request, StudentReqDto studentReqDto) throws SQLException {

        StudentRequest saveStudentRequest = studentRepository.save(studentReqDto.toEntity());

        Long categotyId = studentReqDto.getCategory_id();
        log.info("학생 요청 게시글 작성, categoryId= {}", categotyId);
        StudentRequest studentRequest = studentRepository.findStudentRequestByFundingCategory(categotyId).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST_CATEGORY));
        log.info("학생 요청 게시글 작성, category={}" , studentRequest.getFundingCategory());
        saveStudentRequest.updateCategory(studentRequest.getFundingCategory());

        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");
        String jwtToken = null;
        if (jwtCookie.isPresent()) {
            jwtToken = jwtCookie.get();
        } else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }
        String username = jwtUtil.extractUsername(jwtToken);

        User user = userRepository.findUserByUsername(username).orElseThrow(() ->
                new BaseException(NOT_FIND_USER));
        saveStudentRequest.updateWriter(user);

        return new StudentcreateResDto(saveStudentRequest);
    }


    //게시글 리스트 조회
    @Transactional(readOnly = true)
    public List<StudentRequest> getAllStudentRequest(){
        List<StudentRequest> studentRequests = studentRepository.findAllBy();
        return studentRequests;
    }

    @Transactional(readOnly = true)
    public List<FundingCategory> getStudentRequestCategoryAllList() {
        return fundingCategoryRepository.findAll();
    }

    @Transactional(readOnly = true)
    public FundingCategory getStudentRequestCategoryById(Long categoryId){
        return fundingCategoryRepository.findFundingCategoriesById(categoryId).orElseThrow(() ->
                new BaseException(NOT_FIND_STUDENTREQUEST_CATEGORY));
    }

    @Transactional
    public Long update(Long id, StudentReqDto studentReqDto){
        StudentRequest studentRequest = studentRepository.findById(id).orElseThrow(() ->
                new IllegalArgumentException("해당 게시글이 존재하지 않습니다."));

        studentRequest.update(studentReqDto.getTitle(), studentReqDto.getContent());
        return id;
    }

    @Transactional
    public Long delete(Long id){
        StudentRequest studentRequest = studentRepository.findById(id).orElseThrow(() ->
                new IllegalArgumentException("해당 게시글이 존재하지 않습니다."));
        studentRepository.delete(studentRequest);
        return id;
    }

    //조회수 증가
    @Transactional(readOnly = true)
    public Long updateViewCount(Long studentRequestId){
        StudentRequest studentRequest = studentRepository.findStudentRequestById(studentRequestId).orElseThrow(()
                -> new BaseException(NOT_FIND_STUDENTREQUEST));

        return studentRequest.getViewCount();
    }

    //좋아요 증가
    @Transactional(readOnly = true)
    public Long updateLikeCount(Long studentRequestId){
        StudentRequest studentRequest = studentRepository.findStudentRequestById(studentRequestId).orElseThrow(()
                -> new BaseException(NOT_FIND_STUDENTREQUEST));

        return studentRequest.getLikeCount();
    }

    //게시물 페이징
    @Transactional
    public Page<StudentRequest> pageList(Pageable pageable){
        return studentRepository.findAll(pageable);
    }


    @Transactional
    public String addPostslike(HttpServletRequest request, Long studentRequestId){
        CookieUtil.getCookie(request, "jwtToken");
        Optional<String> jwtCookie = CookieUtil.getCookie(request, "jwtToken");

        String jwtToken = null;
        if(jwtCookie.isPresent()){
            jwtToken = jwtCookie.get();
        }else {
            throw new BaseException(NOT_FIND_LOGIN_SESSION);
        }

        String username = jwtUtil.extractUsername(jwtToken);
        User user = userRepository.findUserByUsername(username).orElseThrow(()
                    -> new BaseException(NOT_FIND_USER));

        PostLike postLike = new PostLike(user, Constant.TargetType.STUDENT_REQUEST, studentRequestId);
        postLikeRepository.save(postLike);
        return "좋아요 저장 완료하였습니다.";

    }

    @Transactional(readOnly = true)
    public List<StudentRequest> OrderByKeyword(String condition){
        //최신
        if (condition == null || condition.equals("newest")){
            return studentRepository.findAllBy();
        }
        //좋아요
        else if (condition.equals("mostLike")) {
            return studentRepository.findByOrderByLikeCountDesc();
        }
        //조회수
        else if (condition.equals("mostView")) {
            return studentRepository.findByOrderByViewCountDesc();
        }else {
            throw new BaseException(NOT_FIND_STUDENTREQUEST);
        }
    }

    //게시글 중에 좋아요 누른 게시글 표시


}
