package com.example.final_project.src.student_request.dto;

import com.example.final_project.src.student_request.entity.StudentComment;
import com.example.final_project.src.user.entity.User;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
public class CommentResDto {

    private String content;
    private String username;
    private Long student_request_id;

    public CommentResDto(StudentComment studentComment){
        this.content = studentComment.getContent();
        this.username = studentComment.getWriter().getUsername();
        this.student_request_id = studentComment.getStudentRequest().getId();
    }
}
