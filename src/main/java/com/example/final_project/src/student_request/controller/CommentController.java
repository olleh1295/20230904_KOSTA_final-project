package com.example.final_project.src.student_request.controller;

import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.common.response.BaseResponseStatus;
import com.example.final_project.src.student_request.CommentService;
import com.example.final_project.src.student_request.dto.CommentReqDto;
import com.example.final_project.src.student_request.dto.CommentResDto;
import com.example.final_project.src.student_request.entity.StudentComment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class CommentController {

    @Autowired
    private CommentService commentService;

    //댓글 리스트(유저)
    @ResponseBody
    @GetMapping("/studentcomment/writer")
    public BaseResponse<List<StudentComment>> getComments(HttpServletRequest request) {
        return new BaseResponse<>(commentService.getStudentCommentByUser(request));
    }

    //댓글 리스트(게시글)
    @ResponseBody
    @GetMapping("/studentcomment")
    public BaseResponse<List<StudentComment>> getCommentsAll(@RequestParam(name = "studentrequest") Long student_request_id) {
        return new BaseResponse<>(commentService.getStudentCommentByStudentRequest(student_request_id));
    }

    //댓글 등록
    @ResponseBody
    @PostMapping("/studentcomment/save")
    public BaseResponse<String> saveStudentRequestComment(HttpServletRequest request, @RequestBody CommentReqDto commentReqDto) {
        return new BaseResponse<>(commentService.insertStudentComment(request, commentReqDto));
    }

    /*댓글 수정
    @ResponseBody
    @GetMapping("/studentcomment/")
    public BaseResponse<String> updateStudentRequestComment(@RequestBody CommentReqDto commentReqDto) {
        return new BaseResponse<>(commentService.updateStudentComment(commentReqDto);
    }

     */
}
