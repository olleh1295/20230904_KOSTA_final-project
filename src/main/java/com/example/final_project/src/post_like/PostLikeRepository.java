package com.example.final_project.src.post_like;

import com.example.final_project.common.Constant;
import com.example.final_project.common.Constant.*;
import com.example.final_project.src.funding_cart.entity.FundingCart;
import com.example.final_project.src.post_like.entity.PostLike;
import com.example.final_project.src.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PostLikeRepository extends JpaRepository<PostLike, Long> {
    Optional<PostLike> findPostLikesByUserAndTargetTypeAndAndTargetId(User user, Constant.TargetType targetType, Long tartgetId);
}
