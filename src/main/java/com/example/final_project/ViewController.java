package com.example.final_project;

import com.example.final_project.common.exceptions.BaseException;
import com.example.final_project.common.response.BaseResponse;
import com.example.final_project.src.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class ViewController {
    @Autowired
    private UserRepository userRepository;
    /**
     * 메인 홈화면
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String toHomePage() {
        return "home";
    }

/*
    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String toHomePage_p() {
        return "main";
    }
*/

    /**
     * 로그인 화면
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String toLoginPage() {
        return "user/login";
    }


    /**
     * 소셜 로그인 콜백 요청 화면
     */
/*    @RequestMapping(value = "/oauth2/{socialLoginType}/callback", method = RequestMethod.GET)
    public String toCallbackPage(@PathVariable(name = "socialLoginType") String socialLoginPath,
                                 @RequestParam(name = "code") String code) {
        return "user/callback";
    }*/

    /**
     * 회원가입 화면
     */
    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String toSignupPage() {
        return "user/signup";
    }


    /**
     * 마이페이지 화면
     */
    @RequestMapping(value = "/mypage", method = RequestMethod.GET)
    public String toMyPage() {
        return "user/mypage";
    }


    /**
     * 펀딩 리스트 화면
     */
    @RequestMapping(value = "/funding", method = RequestMethod.GET)
    public String toFundingPage() {
        return "funding/fundingList";
    }


    /**
     * 펀딩글 작성 화면
     */
    @RequestMapping(value = "/fundingCreate", method = RequestMethod.GET)
    public String toFundingInsertPage() {
        return "funding/fundingCreate";
    }


    /**
     * 펀딩글 상세 화면
     */
    @RequestMapping(value = "/fundingDetail", method = RequestMethod.GET)
    public String toFundingDetailPage(@RequestParam(name = "id") Long funding_id) {
        return "funding/fundingDetail";
    }



    /**
     * 장바구니 화면
     */
    @RequestMapping(value = "/mycart", method = RequestMethod.GET)
    public String toMyCartPage() {return "fundingcart/myCart";}

}
